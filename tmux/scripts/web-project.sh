#!/bin/sh

# Change directory before the commands, if it is provided.
if [ ! -z "$1" ]; then
	cd $1
fi

# Start a new session. Split the first window horizontally, running
# the dev server in the first pane. Then create a second window.
tmux new-session \; \
	send-keys 'yarn dev' C-m \; \
	split-window -h \; \
	neww \;
