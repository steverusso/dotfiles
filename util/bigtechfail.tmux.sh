#!/bin/sh

cd ~/projects/bigtech.fail

tmux new-session -d -s bigtechfail

tmux new-window
tmux send-keys "cd website" C-m
tmux split-window -h

tmux send-keys "cd website" C-m
tmux send-keys "hugo server" C-m

tmux attach-session -t bigtechfail
