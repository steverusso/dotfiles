#!/usr/bin/env bash

# Env vars.
export HISTFILE=~/.zsh_history
export SAVEHIST=2048 
setopt inc_append_history # To save every command before it is executed 
setopt share_history # setopt inc_append_history

export EDITOR=vim
export VISUAL=vim
export LSCOLORS=GxfxbxBxCxfxDxHxHxhxhx
export PATH="$PATH:/usr/local/go/bin:$HOME/go/bin:$HOME/.yarn/bin"

# Color sequence variables.
 black="\e[0;30m"    dgray="\e[1;30m"
   red="\e[0;31m"     lred="\e[1;31m"
 green="\e[0;32m"   lgreen="\e[1;32m"
orange="\e[0;33m"   yellow="\e[1;33m"
  blue="\e[0;34m"    lblue="\e[1;34m"
purple="\e[0;35m"  lpurple="\e[1;35m"
  cyan="\e[0;36m"    lcyan="\e[1;36m"
 lgray="\e[0;37m"    white="\e[1;37m"
reset_color="\e[0m"

# Function _git_info first checks if the current directory is a git repository.
# If it is, the function nicely displays the branch and short status of the repo.
git_info() {
	if git rev-parse --is-inside-work-tree 2> /dev/null | grep -q 'true' ; then
		echo -en "\n${cyan}Branch:${reset_color}"
		echo -e "${blue}`git branch -a | grep '\*' | cut -d'*' -f 2`${reset_color}" 
		git status | sed '2!d' | sed 's/^/  /'

		if [ `git status --short | wc -l` -gt 0 ]; then
			echo -e "\n${cyan}Status:${reset_color}"
			git status --short
		fi

		echo
	fi
}

# Aliases - Misc
alias ..="cd .."
alias l="ls -Alh --color=always"
alias ls="ls --color=always"
alias gogo="goimports -w -l . ; go build && go test"
# Aliases - Git
alias     g="git_info"
alias gitch="git checkout"
alias  gits="git status --short"
alias gitss="git status"
alias  gitd="git diff "
alias gitsd="git status --short && echo && git diff "
alias  gita="git add"
alias  gitA="git add -A "
alias  gitc="git commit -m "
alias  gitC="git add -A && git commit -m "

# Prompt
autoload -U colors && colors
PS1="%{$fg[white]%}[%{$fg_bold[cyan]%}%~%{$reset_color%}]: "

# Plugins.
source ~/.dotfiles/zsh/vi-mode.plugin.zsh
source ~/.dotfiles/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.dotfiles/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Ctrl-P (prev) and Ctrl-N (next) will use zle's history
# search as well as the up and down arrow keys.
bindkey '^P' history-beginning-search-backward
bindkey '^N' history-beginning-search-forward
bindkey '^[[A' history-beginning-search-backward
bindkey '^[[B' history-beginning-search-forward

# Ctrl-U clears entire line.
bindkey \^U kill-whole-line

# Run git info function on shell startup.
g
