#!/bin/sh

# Dependencies
sudo apt update && sudo apt install -y vim tmux zsh

# ZSH
mkdir ~/.dotfiles/zsh

# Vi-mode
curl https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/plugins/vi-mode/vi-mode.plugin.zsh -o ~/.dotfiles/zsh/vi-mode.plugin.zsh
head -n -4 ~/.dotfiles/zsh/vi-mode.plugin.zsh > tmp.zsh
mv tmp.zsh ~/.dotfiles/zsh/vi-mode.plugin.zsh

# Syntax-highlighting and auto-suggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.dotfiles/zsh/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.dotfiles/zsh/zsh-autosuggestions

chsh -s /usr/bin/zsh

# Vim plugin manager
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Link the dotfiles
ln -sf ~/.dotfiles/.zshrc ~/.zshrc
ln -sf ~/.dotfiles/.vimrc ~/.vimrc
ln -sf ~/.dotfiles/tmux/tmux.conf ~/.tmux.conf
