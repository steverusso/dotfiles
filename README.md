
# My .dotfiles

## Installation

First, clone to the repo to `~/.dotfiles`:
```
git clone https://gitlab.com/steverusso/dotfiles.git ~/.dotfiles
```

Once the repo is cloned, run:
```
make [os]
```

Targets exist for `freebsd`, `manjaro`, and `ubuntu`.

