
" General settings
	syntax on
	colorscheme desert
	set dir=/tmp/
	set relativenumber 
	set number
	set nowrap

" Tab settings
	set autoindent
	set tabstop=2
	set shiftwidth=2
	set noexpandtab

" Search settings
	set hlsearch
	nnoremap <C-l> :nohl<CR><C-l>:echo "Search Cleared"<CR>

" Plugins
	call plug#begin('~/.vim/plugged')
		" Syntax
		Plug 'cespare/vim-toml'
		" GPG
		Plug 'jamessan/vim-gnupg'

		" Markdown
		Plug 'plasticboy/vim-markdown'
		let g:vim_markdown_frontmatter = 1
		let g:vim_markdown_json_frontmatter = 1
		let g:vim_markdown_folding_disabled = 1
	call plug#end()
